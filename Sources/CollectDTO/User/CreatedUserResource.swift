import Foundation

public struct CreatedUserResource: Codable {
    public let id: Int
    public let email: String
    public let createdAt: Date
    public let updatedAt: Date
    
    public init(id: Int, email: String, createdAt: Date, updatedAt: Date) {
        self.id = id
        self.email = email
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
