import Foundation

public struct ResetPasswordCommand: Codable {
    public let email: String
    
    public init(email: String) {
        self.email = email
    }
}
