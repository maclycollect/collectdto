import Foundation

public struct LoginTokenResource: Codable {
    public let token: String
    
    public init(token: String) {
        self.token = token
    }
}
