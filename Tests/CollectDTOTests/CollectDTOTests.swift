import XCTest
@testable import CollectDTO

final class CollectDTOTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(CollectDTO().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
