import XCTest

import CollectDTOTests

var tests = [XCTestCaseEntry]()
tests += CollectDTOTests.allTests()
XCTMain(tests)